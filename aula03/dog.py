class Dog():
    '''Tentando abstrair um cachorro'''
    def __init__(self, nome, idade, raca):
        self.nome = nome
        self.idade = idade
        self.raca = raca
        self.energia = 3

    def latir(self):
        self.energia -= 1
        print('auauaua')

    def dormir(self):
        self.energia = 3
        print('Zzzzzzz...')

    def andar(self):
        self.energia -= 1
        print('andando...')
    
    def __str__(self):
        return 'nome: {}, idade: {}, raca:{}'.format(
            self.nome, self.idade, self.raca,
        )


dog1 = Dog('bilu', 2, 'pitbull')
dog2 = Dog('rex', 1, 'poddle')
print(dog1.energia)
dog1.latir()
dog1.andar()
print(dog1.energia)
dog1.__doc__
# print(dir(dog1))
# print(dog1)
# print(dog1.nome)
# dog1.latir()
