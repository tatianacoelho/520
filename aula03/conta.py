class Conta():
    '''Tentando abstrair uma conta'''

    def __init__(self, titular, numero, saldo):
        self.titular = titular
        self.numero = numero
        self.saldo = saldo

    def sacar(self, valor):
        self.saldo -= valor
        return self.saldo

    def depositar(self, valor):
        self.saldo += valor
        return self.saldo

    def transferir(self, valor, conta):
        self.sacar(valor)
        conta.depositar(valor)

    def __str__(self):
        return '{},{},{} CC'.format(
            self.titular, self.numero, self.saldo
        )


class Poupanca(Conta):
    def __init__(self, titular, numero, saldo):
        super().__init__(titular, numero, saldo)
        self.juros = 0.005

    def render_juros(self):
        self.saldo += self.saldo * self.juros
        return self.saldo

    def __str__(self):
        return '{}, {}, {} Poupança'.format(
            self.titular, self.numero, self.saldo
        )


