from pymongo import MongoClient, DESCENDING
import time

try:
    con = MongoClient('mongodb://username:password@host1:port1/base')
    db = con['projeto1']
except Exception as e:
    print('Erro: {}'.format(e))
    exit()


def cadastrar(nome, mensagem):
    date = {
        'nome': nome,
        'mensagem': mensagem,
        'hora': time.strftime('%d-%m-%Y %H:%M:%S')
    }
    db.chat.insert(date)


def ler_msg():
    ultimo = [x for x in db.chat.find().sort('_id', DESCENDING)]
    while True:
        date = [x for x in db.chat.find().sort('_id', DESCENDING)]
        if date != ultimo:
            ultimo = date
            print('[{}] {} : {}\n'.format(
                date[0]['hora'], date[0]['nome'], date[0]['mensagem']
            ))
        time.sleep(2)
