from unittest import TestCase, main


def raiz(num):
    try:
        return int(num) ** 0.5
    except Exception:
        return False


def soma(x, y): return x + y


class Raiz(TestCase):
    def test_raiz(self):
        self.assertEqual(raiz(64), 8)
        self.assertEqual(raiz(25), 5)
        self.assertEqual(raiz(36), 6)
        self.assertEqual(raiz(81), 9)
        self.assertEqual(raiz('64'), 8)
        self.assertEqual(raiz('absda'), False)

    def test_soma(self):
        self.assertEqual(soma(2, 2), 4)


if __name__ == '__main__':
    main()
